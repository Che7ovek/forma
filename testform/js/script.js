(window => {
	'use strict';

	if (!!window.TestForm)
		return;

	if (!!window.QuickModal)
		return;

	window.TestForm = class TestForm {
		constructor(obIds) {
			this.obIds = obIds;

			let arIds = Object.entries(this.obIds).map(row => this.getNode(row[1]));
			/*
			 * Найдём все Элементы по id
			 * Если не найдём - не запустим ничего
			 */
			Promise.all(arIds)
				.then(nodes => nodes.forEach(
					node => this['ob' + node.id] = node
				))
				.then(() => {
					this.init()
				})
				.catch(error => {
					console.log(error.message);
			});
		}

		init() {
			// Кнопка активна, если все элементы на месте
			this[`ob${this.obIds["SUBMIT"]}`].disabled = false;
			document.addEventListener("ymapsCoords", event => {
				this[`ob${this.obIds["ADDRESS2"]}`].value = event.detail.coords;
			});
			document.addEventListener("submit", this.onSubmit.bind(this));
		}

		onSubmit(event) {
			event.preventDefault();
			event.stopPropagation();

			Promise.all([
				this.validate(this[`ob${this.obIds["NAME"]}`]),
				this.validate(this[`ob${this.obIds["PHONE"]}`]),
				this.validate(this[`ob${this.obIds["ADDRESS1"]}`]),
				this.validate(this[`ob${this.obIds["ADDRESS2"]}`])
			])
				.then(values => this.preparePost(values))
				.then(options => fetch('',options))
				.then(response => this.afterPost(response))
				.then(txt => this.cleanForm(txt))
				.catch(error => {
					console.log(error.message);
			});
		}

		async getNode(nodeId) {
			return new Promise((resolve, reject) => {
				let node = document.getElementById(nodeId);
				if(!node)
					reject(new Error('Нет элемента ' + nodeId));

				resolve(node);
			});
		}

		async validate(node) {
			return new Promise((resolve, reject) => {
				let testReg,
					isGood = false;

				switch (node.id) {
					case this.obIds["NAME"]:
						/*
						 * Давайте сразу вместе с тире разрешим минус, дефис и длинное тире.
						 * Ничего страшного они не сделают (хотя можно ещё перед валидацией символы заменять, но тут уж как хотите)
						 */
						testReg = /^[a-zA-Zа-яА-Я\-–—−]{0,25}$/;
						isGood = testReg.test(node.value);
						break;
					case this.obIds["PHONE"]:
						testReg = /^[\d\(\)\s\-+–—−]{1,25}$/;
						isGood = testReg.test(node.value);
						break;
					default:
						isGood = !!node.value;
				}

				if(isGood)
				{
					node.classList.remove('bad');
					resolve(node);
				}
				else
				{
					node.classList.add('bad');
					reject(new Error(`Ошибка в ${node.id}`));
				}
			});
		}

		async preparePost(nodes) {
			const postData = new FormData();
			let options = {};

			nodes.map(node => {postData.append(node.name, node.value)});
			postData.append(this.obIds["SESSID"], this[`ob${this.obIds["SESSID"]}`].value);
			/*
			 * Хардкодить нехорошо.
			 * Можно из локализации битриксовской передавать сообщения в BX.message и оттуда же выводить
			 * Но я тут решил вообще без битриксовской библиотеки обойтись
			 * Можно просто в инициализации new TestForm передавать массив сообщений в переменную
			 * Много чего можно, но сегодня я хардкожу
			 */  
			this[`ob${this.obIds["SUBMIT"]}`].value = "Идёт отправка";
			this[`ob${this.obIds["SUBMIT"]}`].disabled = true;

			options = {
				method: 'POST',
				body: postData
			};

			return options;
		}

		async afterPost(response) {
			this[`ob${this.obIds["SUBMIT"]}`].value = "Отправка";
			this[`ob${this.obIds["SUBMIT"]}`].disabled = false;

			if(response.status == 200)
			{
				new QuickModal('Успешно отправлено');
				return response.text();
			} else {
				new QuickModal('Произошла ошибка');
				throw new Error('Произошла ошибка');
			}
		}

		async cleanForm(text) {
			this[`ob${this.obIds["FORM"]}`].reset();
			// reset не затирает hidden поля (и хорошо, у нас там sessid)
			this[`ob${this.obIds["ADDRESS2"]}`].value = '';
			document.dispatchEvent(new CustomEvent("ymapsClean"));
		}
	}

	window.QuickModal = class QuickModal {

		constructor(message) {
			// Мне больше одного попапа не надо
			const instance = this.constructor.instance;
			if (instance)
				return instance;

			this.constructor.instance = this;
			this.element = document.createElement('div');
			this.element.className = 'modal';
			this.element.innerHTML = `<div class="inner">${message}</div>`;
			document.body.prepend(this.element);
			// Чтоб сработала анимация
			setTimeout(() => {this.element.classList.add('show');}, 100);
			setTimeout(this.destructor.bind(this), 2100);
		}

		destructor() {
			this.element.classList.remove('show');
			// Удаляем элемент после анимации
			setTimeout(() => {
				this.element.remove();
				this.constructor.instance = null;
			}, 1000);
		}
	}
})(window);