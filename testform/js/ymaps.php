<script>
	ymaps.ready(init);
	function init(){

		var myPlacemark;
		var myMap = new ymaps.Map("map", {
			center: [59.93993072, 30.31988361],
			controls: [],
			zoom: 11
		});
		//темплейт для балуна
		var formBalloonContentLayoutClass = ymaps.templateLayoutFactory.createClass(
			'<div class="formMapBalloon">' + 
			'<h3>{{ properties.name }}</h3>' +
			'<span>{{ properties.coords }}</span>' +
			'</div>'
		);
		ymaps.layout.storage.add('testForm#simplestBCLayout', formBalloonContentLayoutClass);
		// Слушаем клик на карте.
		myMap.events.add('click', function (e) {
			var coords = e.get('coords');
			// Если метка уже создана – просто передвигаем ее.
			if (myPlacemark) {
				myPlacemark.geometry.setCoordinates(coords);
			}
			// Если нет – создаем.
			else {
				myPlacemark = createPlacemark(coords);
				myMap.geoObjects.add(myPlacemark);
				// Слушаем событие окончания перетаскивания на метке.
				myPlacemark.events.add('dragend', function () {
					//Обратное геокодирование доступно только по API-ключу. Кто б мне его дал для локальной установки
					//getAddress(myPlacemark.geometry.getCoordinates());

					document.dispatchEvent(new CustomEvent("ymapsCoords", {
						detail: { coords: coords }
					}));
				});
			}
			// "Передаём" координаты в форму
			document.dispatchEvent(new CustomEvent("ymapsCoords", {
				detail: { coords: coords }
			}));
			//getAddress(coords);
		});

		document.addEventListener("ymapsClean", event => {
			myMap.geoObjects.removeAll();
			myPlacemark = undefined;
		});

		// Создание метки.
		function createPlacemark(coords) {
			return new ymaps.Placemark(coords, {
				name: 'Координаты',
				coords: coords,
			}, {
				preset: 'islands#blackDotIcon',
				draggable: true,
				balloonContentLayout:  'testForm#simplestBCLayout'
			});
		}

		// Определяем адрес по координатам (обратное геокодирование).
		/*
		 * Стандартная функция из документации. Не использую, но пусть лежит.
		 * Поменял только свойство, куда результаты выводятся. Должно работать, если появится ключ (но в форме всё равно координаты)
		 */
		function getAddress(coords) {
			myPlacemark.properties.set('iconCaption', 'поиск...');
			ymaps.geocode(coords).then(function (res) {
				var firstGeoObject = res.geoObjects.get(0);
		
				myPlacemark.properties
					.set({
						// Формируем строку с данными об объекте.
						coords: [
							// Название населенного пункта или вышестоящее административно-территориальное образование.
							firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
							// Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
							firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
						].filter(Boolean).join(', '),
						// В качестве контента балуна задаем строку с адресом объекта.
						balloonContent: firstGeoObject.getAddressLine()
					});
			});
		}

		// Адресовые подсказки
		var suggestView = new ymaps.SuggestView('<?=$arParams["ADDRESS_ID"]?>');
	}
</script>