<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/*
 * Планировал сначала сделать отправку данных через BX.ajax
 * Но подумал: "Один раз живём, сделаю на ваниле"
 */
//CJSCore::Init();
?><!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<title>Тестовая форма</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
<?
	// Не использую Asset::getInstance()->addString, и подобные, потому что нет вызова ShowCss и остальных.
	// А их не вызываю, потому что они подягивают файлы шаблона, который мне здесь не нужны
?>
	<link href='./css/styles.css' rel='stylesheet' type='text/css'>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>