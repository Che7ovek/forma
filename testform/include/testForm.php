<?
namespace leonidLeonid;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\IO,
	Bitrix\Main\Context;

Loc::loadLanguageFile(__FILE__, 'ru');

class testForm {
	const FIELDS = [
		'NAME',
		'PHONE',
		'ADDRESS1',
		'ADDRESS2'
	];

	public static function doStuff($arIds) {
		if(!is_array($arIds) || empty($arIds))
			return;

		$arValues = [];
		$request = Context::getCurrent()->getRequest();
		foreach(self::FIELDS as $name)
		{
			$value = trim($request->getPost($arIds[$name]));
			if($value !== '')
			{
				self::sanitizeValue($value);
				$arValues[] = Loc::getMessage('TF_'.$name) . $value;
			}
		}
		if(!empty($arValues))
		{
			$arValues[] = Loc::getMessage('TF_IP') . self::getIP();
			$strValue = implode(";\n", $arValues) . ";\n\n";
			// Логи пишутся в папку, в которой лежит index.php
			$file = new IO\File(dirname(__DIR__, 1) . "/logform.txt");
			$file->putContents($strValue, IO\File::APPEND);
			return 'балдёж';
		}
		return 'отстой';
	}

	protected static function sanitizeValue(&$value)
	{
		global $DB;
		// Без коннекта mysqli_real_escape_string не работает
		$DB->DoConnect();
		// А вдруг всё-таки не MySQLi
		if(function_exists("mysqli_real_escape_string"))
			$value = mysqli_real_escape_string($DB->db_Conn,$value);
		else
			$value = self::fakeEscapeString($value);
	}


	protected static function fakeEscapeString(&$value)
	{
		$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
		$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
	
		return str_replace($search, $replace, $value);
	}

	protected static function getIP()
	{
		$ip = '';
		$server = Context::getCurrent()->getServer();
		$client  = $server->get('HTTP_CLIENT_IP');
		$forward = $server->get('HTTP_X_FORWARDED_FOR');
		$remote  = $server->get('REMOTE_ADDR');
		
		if(filter_var($client, FILTER_VALIDATE_IP))
			$ip = $client;
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
			$ip = $forward;
		else
			$ip = $remote;

		return $ip;
	}
}