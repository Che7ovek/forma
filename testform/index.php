<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Context;

$arIds = [
	'FORM'		=> 'form',
	'NAME'		=> 'name',
	'PHONE'		=> 'phone',
	'ADDRESS1'	=> 'address1',
	'ADDRESS2'	=> 'address2',
	'SUBMIT'	=> 'submit',
	'SESSID'	=> 'sessid',
];

$server = Context::getCurrent()->getServer();
define('THIS_SUB_DIR', substr(__DIR__, strlen($server->getDocumentRoot())));

if($server->getRequestMethod() === 'POST')
{
	if(check_bitrix_sessid($arIds['SESSID']))
	{
		require_once(__DIR__ . "/include/testForm.php");
		$result = leonidLeonid\testForm::doStuff($arIds);
		echo $result;
	}
	die();
}

require_once(__DIR__ . "/include/head.php");
?>
<body>
	<div id="modal"></div>
	<main>
		<h1>Форма</h1>
		<form method='POST' id="<?=$arIds['FORM']?>" novalidate>
			<?=bitrix_sessid_post($arIds['SESSID'])?>
			<div class="form-row">
				<label for='name'>Имя</label>
				<input type="text" name="<?=$arIds['NAME']?>" id="<?=$arIds['NAME']?>"/>
				<span class="hint">Имя должно содержать только буквы</span>
			</div>
			<div class="form-row">
				<label for='phone'>Телефон <span>*</span></label><?
				/*
				 * Не настраивал маску, потому что для этого существуют специализированные плагины (вроде intl-tel-input.js)
				 * А если найти список масок по странам, то можно и через iMask.js всё запросто сделать (https://imask.js.org/guide.html#masked-dynamic)
				 * Но мне же вроде как надо самому всё сделать, не?
				 */
				?><input type="tel" name="<?=$arIds['PHONE']?>" id="<?=$arIds['PHONE']?>" required/>
				<span class="hint">Некорректный телефон</span>
			</div>
			<div class="form-row">
				<label for='address1'>Адрес 1 <span>*</span></label>
				<input type="text" name="<?=$arIds['ADDRESS1']?>" id="<?=$arIds['ADDRESS1']?>" required/>
				<span class="hint">Обязательное поле</span>
			</div>
			<div class="form-row">
				<label for='address2'>Адрес 2 <span>*</span></label>
				<input type="hidden" name="<?=$arIds['ADDRESS2']?>" id="<?=$arIds['ADDRESS2']?>" value="" required>
				<span class="hint">Обязательное поле</span>
				<div class="map" id="map" style="width: 100%; height: min(60vw, 400px);"></div>
			</div>
			<div class="form-row controls">
				<input type="submit" id="<?=$arIds['SUBMIT']?>" value="Отправить" disabled>
			</div>

		</form>
	</main>
<?
/*
 * Не совсем правильно, конечно, кидать JS в php файл и засовывать его в папку js
 * Но прям в этом файле оно лишнее
 * И это всё-таки js, так что в папке js ему самое место
 * И мне нужна php переменная в скрипте, а так проще всего её туда отправить
 */
$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"", array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => THIS_SUB_DIR."/js/ymaps.php",
		"ADDRESS_ID" => $arIds["ADDRESS1"]
	),
	false
);?>
<script src="./js/script.js"></script>
<script>
	let testForm = new TestForm(<?=CUtil::PhpToJSObject($arIds)?>);
</script>
</body>